# LED_STATUS_CIRCUIT

This circuit provides feedback for the user of the uHAT.

Initially when the battery is full or above 75%, all five LEDs will be on.

As the battery is used up to below 75% but above 53%, the two green LEDs will turn off.

Only the two blue and one red LEDs will be on, showing that the battery is getting used up. 

As the battery goes below 53% the two blue LEDs will also turn off and only the red LED on. 

When the battery gets used up to below 20%, the remaining red LED will also turn off.

No LED/s is/are on at this point, meaning the battery is more than 80% used up. 
 
This is a good indication that the microHAT power is about to be cut-off.

The microHAT needs to be manually switch off to avoid any damages caused by power-cuts. 

 
